# Problem 5 (Smallest multiple)

# 2520 is the smallest number that can be divided by each of the numbers
#   from 1 to 10 without any remainder.
# What is the smallest positive number that is evenly divisible by all of
#   the numbers from 1 to 20?

# Algorithm:
# Break down each composite into its unique prime factorization
#  if this is a subset of the result set, move on, otherwise add
#  necessary elements to result set so it contains this subset

require './helpers/primes'

MAX_NUM  = 20

# Remove elements from curr that exist in res, then add remaining elements
#   from curr to the result
#   (eg: merge_factors([2, 3], [3, 3, 4]) # => [2, 3, 3, 4])
def merge_factors(res, curr)
  res.each { |n| (i = curr.index(n)) && curr.slice!(i) }
  res + curr
end

res = []
primes = Primes.primes(MAX_NUM)
MAX_NUM.downto(2).each do |n|
  res = merge_factors(res, Primes.factorization(primes, n))
end

puts res.inspect
puts res.inject(:*)
