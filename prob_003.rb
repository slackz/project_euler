# Problem 3 (Largest prime factor)

# The prime factors of 13195 are 5, 7, 13 and 29.
# What is the largest prime factor of the number 600851475143 ?

## NOTE!
##   This implementation requires tail call optimization, to compile this into
##   ruby, set the following constants to the values below in vm_opts.h
##       #define OPT_TRACE_INSTRUCTION        0
##       #define OPT_TAILCALL_OPTIMIZATION    1


# Fundamental theorem of arithmetic:
#  Every integer > 1 is either prime or has a unique prime factorization

# Algorithm:
#   1. Start with current largest number, smallest prime (2)
#   2. Return if cur num == cur divisor || cur_num <=2
#   2. Get cur_num / cur_divisor and cur_num % cur_divisor
#   3. - if modulus is zero go to step 2. with quotient and smallest prime
#      - else go to step2 with cur num and cur_divisor + 1
MAX_NUM = 600851475143

def largest_prime(cur_num, cur_divisor)
  return cur_num if (cur_num == cur_divisor || cur_num <= 2)
  quotient = cur_num / cur_divisor
  rem = cur_num % cur_divisor
  rem.zero? ? largest_prime(quotient, 2) :
    largest_prime(cur_num, cur_divisor + 1)
end

puts largest_prime(MAX_NUM, 2)
