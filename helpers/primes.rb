
module Primes

  # Reasonable for relatively small result sets...
  # Because this sieve builds up arrays, it quickly becomes fairly inefficient
  def self.primes(max_num)
    arr = (2..max_num).to_a
    remove_multiples(arr, 0)
  end

  def self.factorization(poss_primes, n)
    def self.fact_helper(poss_primes, res, n)
      return (res << n) if poss_primes.include?(n)
      p = poss_primes.first
      (n % p == 0) ? fact_helper(poss_primes, (res << p), (n / p)) :
        fact_helper(poss_primes[1..-1], res, n)
    end

    fact_helper(poss_primes, [], n)
  end

  private

  def self.remove_multiples(arr, index)
    return arr if index + 1 >= arr.size
    elem    = arr[index]
    new_arr = arr[0..index] + arr[(index + 1)..-1].reject { |n| n % elem == 0 }
    remove_multiples(new_arr, index + 1)
  end

end
