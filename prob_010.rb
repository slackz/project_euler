# https://projecteuler.net/problem=10

# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
# Find the sum of all the primes below two million.

# Note: This is slow; about 8 minutes to run...
#  TODO: look into better algos for prime num generation

primes = [2]
n = 3

while(n < 2_000_000) do
  is_prime = !primes.any? { |prime| n % prime == 0 }
  primes << n if is_prime
  puts n if (n - 1) % 10_000 == 0
  n += 2
end

puts primes.inject(:+)
