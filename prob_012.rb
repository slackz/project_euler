# https://projecteuler.net/problem=12

# First triangle number with more than 500 divisors
DESIRED_FACTORS = 501


def num_factors(n)
  count = 2 # includes 1 and n
  sqrt  = Math.sqrt(n)

  2.upto(sqrt) { |i| count += 2 if n % i == 0 }
  count
end

tri_num, i = 1, 1
while(num_factors(tri_num) < DESIRED_FACTORS) do
  i += 1
  tri_num += i
end

puts tri_num
